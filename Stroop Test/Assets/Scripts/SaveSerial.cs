using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// class for saving and loading the highscore
public class SaveSerial : MonoBehaviour
{
    // the variables with new values to be saved, or loaded old values
    public string timeTextToSave;   // highscore as displayed to the user
    public float timeFloatToSave;   // highscore as a float

    // function to save the highscore data
    public void SaveScore()
    {
        // creates a directory for the save data if one does not already exist
        if (!Directory.Exists("Saves"))
        {
            Directory.CreateDirectory("Saves");
        }

        // create new binary formatter, file, and save data 
        BinaryFormatter bf = new BinaryFormatter();
        FileStream saveFile = File.Create("Saves/save.binary");
        SaveData data = new SaveData();

        // set the save data variables to the values being saved
        data.savedTimeText = timeTextToSave;
        data.savedTimeFloat = timeFloatToSave;

        // convert the save data to binary and store in save file
        bf.Serialize(saveFile, data);
        saveFile.Close();
        print("Highscore saved.");
    }

    public void LoadScore()
    {
        // if a save file exists
        if (File.Exists("Saves/save.binary"))
        {
            // open save file and deserialize the data
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open("Saves/save.binary", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);
            file.Close();

            // set the current variables to the values from the saved data
            timeTextToSave = data.savedTimeText;
            timeFloatToSave = data.savedTimeFloat;
            print("Highscore data found.");
        }
        else   // there is no save file
        {
            print("Error: No highscore save data found.");
        }
    }
}

[Serializable]
class SaveData
{
    // the variables in the save file
    public string savedTimeText;    // saved highscore as displayed
    public float savedTimeFloat;    // saved highscore as a float
}
