using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestManager : MonoBehaviour
{
    private WordRandomiser wordRandomiser;      // randomises the word and color variables

    // values for reducing rounds in test
    public int maxNumOfRounds = 5;      // maximum number of rounds
    private int currRoundIndex = 1;     // index of current round

    // scoring variables
    private int numRight = 0;   // number of correct answers
    private int numWrong = 0;   // number of incorrect answers
    private float timerFloat;   // duration of the test

    // time score display variables
    private string timerString;     // value of test duration in desired display format
    private int mins;               // minute passed
    private int secs;               // second passed
    private float mills;            // milliseconds passed

    // text objects to display score variables
    public Text rightDisplay;       // correct answers score
    public Text wrongDisplay;       // incorrect answers score
    public Text timerDisplay;       // test duration

    // color value for the correct answer
    public VISUAL_COLOR correctColor;

    // results screen variables
    public GameObject gameCanvas;       // test canvas object (to be turned off when results screen shows)
    public GameObject resultCanvas;     // results canvas object
    public Text resultDisplay;          // text object for test results
    public Text highscoreDisplay;       // text object for highscore value
    
    // manager for saving and loading highscore data
    private SaveSerial data;

    // Start is called before the first frame update
    private void Start()
    {
        wordRandomiser = GetComponent<WordRandomiser>();

        data = GetComponent<SaveSerial>();
        data.LoadScore();
        highscoreDisplay.text = data.timeTextToSave;
    }

    // Update is called once per frame
    private void Update()
    {
        if (gameCanvas.activeInHierarchy)   // if the test is active,
        {
            UpdateTimer();      // update the test duration timers
            DisplayTimer();     // update the test duration display
        }
    }

    // function to update the test duration variables
    private void UpdateTimer()
    {
        timerFloat += Time.deltaTime;   // increment the total timer by delta time
        
        mills += Time.deltaTime;        // increment the milliseconds by delta time
        if (mills >= 1.0f)              // after 1000 milliseconds,
        {
            secs++;                     // increment the seconds,
            mills = 0;                  // reset milliseconds.
        }

        if (secs >= 60)     // after 60 seconds,
        {
            mins++;         // increment the minutes,
            secs = 0;       // reset seconds.
        }
    }

    // function to update the display for the test duration
    private void DisplayTimer()
    {
        // create strings containing the time values in the correct display format
        string sMin = ConvertTimeToString(mins);                        // minutes
        string sSec = ConvertTimeToString(secs);                        // seconds
        string sMill = ConvertTimeToString((int)(mills * 100.0f));      // milliseconds

        timerString = sMin + ":" + sSec + ":" + sMill;      // set the timer string variable containing the above values in the 00:00:00 format
        timerDisplay.text = timerString;                    // set the timer text object to display the new timerString
    }
    
    // function to convert timer values to their display format and return it as a string
    private string ConvertTimeToString(int val)
    {
        string stringVal = val.ToString();      // create a string containing val
        if (val < 10)                           // if val is less than 10,
        {
            stringVal = "0" + val;              // diplay a "0" before val in the string (keeps timer displaying in a consistant format - 00:00:00)
        }

        // return the string containing the time value
        return stringVal;
    }

    // function to check the user's answer against the correct answer
    public void CheckAnswer(ColorAnswerButton answer)
    {
        // if the user answer matches the correct answer, increment the correct answer counter and update its display.
        if (answer.color == correctColor)
        {
            numRight++;
            rightDisplay.text = "Correct: " + numRight.ToString();
        }
        // if the user answer does not match the correct answer, increment the incorrect answer counter and update its display.
        else
        {
            numWrong++;
            wrongDisplay.text = "Incorrect: " + numWrong.ToString();
        }

        // if the current round index is less than the maximum rounds, generate a new word and increment the round index.
        if (currRoundIndex < maxNumOfRounds)
        {
            wordRandomiser.GenerateNewWord();
            currRoundIndex++;
        }
        else
        {
            // if the user answered all rounds correctly, check if highscore has been beaten and update.
            if (numRight == maxNumOfRounds)
            {
                UpdateHighScore();
            }

            // display the results screen.
            DisplayScore();
        }
    }

    // function to display the results screen
    private void DisplayScore()
    {
        // turn off the test canvas and activate the results canvas
        gameCanvas.SetActive(false);
        resultCanvas.SetActive(true);

        // display the user's score
        resultDisplay.text = numRight + "/" + maxNumOfRounds;

        // player scored more than 50%
        if (numRight > numWrong)
        {
            // player wins, add congratulatory message
            resultDisplay.text += " Great job!!!";
        }

        // player scored less than 50%
        else if (numRight < numWrong)
        {
            // player loses, add encouraging message
            resultDisplay.text += " Better luck next time!";
        }

        // player scored exactly 50%
        else
        {
            // tie, add encouraging message
            resultDisplay.text += " So close - try again!";
        }
    }

    // function to check if the highscore has been beaten and update it
    private void UpdateHighScore()
    {
        // get saved highscore
        SaveSerial data = GetComponent<SaveSerial>();
        data.LoadScore();

        // if there's a highscore
        if (data.timeTextToSave != "")
        {
            // if the highscore is beaten
            if (data.timeFloatToSave > timerFloat)
            {
                // save the current score as the highscore
                data.timeFloatToSave = timerFloat;
                data.timeTextToSave = timerString;
                data.SaveScore();
            }

            // update the highscore display text
            highscoreDisplay.text = data.timeTextToSave;
        }
        // if there is no highscore
        else
        {
            // save the current score as the highscore
            data.timeFloatToSave = timerFloat;
            data.timeTextToSave = timerString;
            data.SaveScore();

            // update the highscore display text
            highscoreDisplay.text = data.timeTextToSave;
        }
    }

    // function called by restart button to re-load the test scene
    public void RestartTest()
    {
        SceneManager.LoadScene(1);
    }

    // function called by exit button to close the application
    public void ExitApplication()
    {
        Application.Quit();
    }
}
