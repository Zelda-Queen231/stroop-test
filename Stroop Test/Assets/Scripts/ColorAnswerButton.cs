using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VISUAL_COLOR    // enum used to identify colors
{
    RED,
    BLUE,
    YELLOW,
    GREEN,
}

public class ColorAnswerButton : MonoBehaviour
{
    // The color the button will provide as the user's answer
    public VISUAL_COLOR color;
}
