using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordRandomiser : MonoBehaviour
{
    private TestManager testManager;    // scene manager
    public Text display;                // text object that displays the randomised word and color

    // arrays holding the options for the randomised variables
    public string[] colorNames = { "Red", "Blue", "Yellow", "Green" };                                                  // word array
    public VISUAL_COLOR[] colors = { VISUAL_COLOR.RED, VISUAL_COLOR.BLUE, VISUAL_COLOR.YELLOW, VISUAL_COLOR.GREEN };    // color array

    // current values of the randomised variables
    private VISUAL_COLOR currColor;     // text color
    private string currWord;            // text string

    // array indices for the current randomised variables
    private int currWordIndex;      // word index
    private int currColorIndex;     // color index

    // bool to indicate whether the color variable has been randomised
    private bool colorRandomised;

    // Start is called before the first frame update
    void Start()
    {
        testManager = GetComponent<TestManager>();  // scene manager
        GenerateNewWord();  // randomise the variables to start the test
    }

    // function for generation new values for the randomised variables
    public void GenerateNewWord()
    {
        RandomiseWord();        // randomise word variable
        RandomiseColor();       // randomise color variable
        UpdateDisplay();        // update the word and color displayed to the player
    }

    // function for randomising the word variable
    private void RandomiseWord()
    {
        currWordIndex = Random.Range(0, colorNames.Length);     // generate a random index in the word array
        currWord = colorNames[currWordIndex];                   // set the word variable to the element at that index
    }

    // function for randomising the color variable
    private void RandomiseColor()
    {
        colorRandomised = false;                                        // set bool to false (color has not been randomised yet)
        
        // keep randomising the color variable until it does not match the word variable 
        while (!colorRandomised || currColorIndex == currWordIndex)     // while the color index matches the word index (or it hasn't been randomised yet)
        {
            currColorIndex = Random.Range(0, colors.Length);    // generate a random index in the color array
            currColor = colors[currColorIndex];                 // set the color variable to the element at that index
            colorRandomised = true;                             // indicate that color has been randomised
        }
    }

    // function to update the display text object
    private void UpdateDisplay()
    {
        testManager.correctColor = currColor;       // set the scene manager's correct color to the current color selected
        display.text = currWord;                    // set the display text to the current word variable

        // set the display color to the one corresponding to the current color variable 
        switch (currColor)
        {
            // set display color to red
            case VISUAL_COLOR.RED:
                display.color = Color.red;
                break;

            // set display color to blue
            case VISUAL_COLOR.BLUE:
                display.color = Color.blue;
                break;

            // set display color to yellow
            case VISUAL_COLOR.YELLOW:
                display.color = Color.yellow;
                break;

            // set display color to green
            case VISUAL_COLOR.GREEN:
                display.color = Color.green;
                break;
        }
    }
}
