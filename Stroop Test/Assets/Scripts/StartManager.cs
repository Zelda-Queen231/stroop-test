using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartManager : MonoBehaviour
{
    // highscore display text object
    public Text highscore;

    // Start is called before the first frame update
    void Start()
    {
        SaveSerial data = GetComponent<SaveSerial>();   // get the save data manager,
        data.LoadScore();                               // load save data,
        highscore.text = data.timeTextToSave;           // display saved highscore.
    }

    // function called by start button to load the test scene
    public void LoadTestScene()
    {
        SceneManager.LoadScene(1);
    }

    // function called by exit button to close the application
    public void ExitApplication()
    {
        Application.Quit();
    }
}
